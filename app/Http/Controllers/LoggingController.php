<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoggingController extends Controller
{
    /**
     * Create new log entry after the search query.
     *
     * @return Response
     */
    public function createLog(Request $request, String $movieName) {

        DB::table('logging')->insert([
            'user_agent' => $request->header('user-agent'), 
            'visitor_ip' => $request->ip(),
            'query' => $movieName,
        ]);
    }
}
