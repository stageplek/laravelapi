<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\LoggingController;
use DB;

class MovieController extends Controller
{
    /**
     * Read input query, then looks if the data is in cache otherwise it will grab the new data.
     *
     * @return Response
     */
    public function readMovie(Request $request, String $movieName) {
        
        $LoggingController = new LoggingController();

        $movieData= Cache::get($movieName);

        if ($movieData !== null) {

            $LoggingController->createLog($request, $movieName);
            return response()->json($movieData);

        } else {

            $LoggingController->createLog($request, $movieName);
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://api.tvmaze.com/search/shows?q=' . $movieName);
            $movieData = $response->getBody()->getContents();
            Cache::put($movieName, $response, now()->addMinutes(4320));
            return response()->json($movieData);
        }           
    }
}
