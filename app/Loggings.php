<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loggings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'visitor_ip', 
        'user_agent',
        'created_at',
        'updated_at',
    ];

    public function logs()
    {
        return $this->hasMany('App\Loggings');
    }
}
